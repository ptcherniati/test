# Lexique

Vous trouverez ici une liste des termes immuables, se trouvant dans le _fichier de configuration_, accompagnés d'une définition et d'exemple.

Ces termes seront classés par catégorie : 

- description de l'application
- description des référentiels
- description de référentiels composés
- description des types de données

## OpenADOM 
__version :__ nous l'utilisons pour la version de `OpenADOM` (actuellement, c'est la version 1).

## L'application
__application :__ dans cette partie nous décrirons l'application.

- __version :__ Nous changeons la version de l'application avant d'importer les _fichiers de configuration_ sur `OpenAdom`.

- __defaultLanguage :__ On peut définir ici une langue d'affichage par défaut en utilisant les [abréviations](https://www.npmjs.com/package/i18n-locales) du nom de la langue. 
(ex : français -> fr, anglais -> en)

- __name :__ Le nom de l'application qui sera enregistré en base de données.

- __internationalizationName__<a id = "internationalizationName" />" : L'affichage du  nom (__name__) en fonction de la langue :
  ```yaml
  internationalizationName:
    en: expression en anglais
    fr: expression en français
    ...: 
  ```

## Les référentiels (_references_)
__references :__ Un ensemble d'informations permettant de préciser le contexte de la mesure ou de l'observation. 

En déportant ces informations dans des fichiers __references__, on évite la répétition d'informations. On utilisera la clef d'une information pour y faire référence.

- __internationalizationName :__ (cf. [internationalizationName](internationalizationName))
- __internationalizedColumns :__ permet de relier des colonnes contenant une information et ses traductions

- __internationalizationDisplay :__ permet de définir pour chaque langue comment le référentiel s'affiche dans l'interface.
  - __pattern:__ L'expression qui génère la chaîne d'affichage.
  ```yaml
  pattern:
    en: "the name: {colonneMonNom}"
    fr: "le nom: {colonneMonNom}"
    ...:
  ```
  
- __keyColumns :__ Un tableau des noms des colonnes faisant partie de la clef primaire.
### Les colonnes du fichier

  __columns :__ Chaque _columns_ est une colonne se trouvant dans le fichier du référentiel. Par conséquent, elle doit avoir le même nom que dans le fichier.
  - __checker__ permet de valider une colonne ou préciser son format (cf [checker](checker))
  
  - __defaultValue__ : la valeur par défaut si aucune valeur n'est fournie. (cf. [groovy](groovy))
  
  - __presenceConstraint<a id="presenceConstraint" />__ : Pour dire si la colonne doit être présente dans le fichier (**MANDATORY**), ou si on peut l'omettre (**OPTIONAL**)

### Les colonnes calculées
__computedColumns :__ Une _computedColumns_ est une colonne qui n'est pas présente dans le fichier et dont la valeur est une constante ou le résultat d'un calcul.
  - __checker__ permet de valider une colonne ou préciser son format (cf [checker](checker))

  - __computation<a id ="computation" />:__ section de calcul de la donnée 
    - __groovy__ (cf. [groovy](groovy))
    - __references__: un tableau des référentiels à ajouter au contexte de calcul
    - __datatypes__: un tableau des types de données à ajouter au contexte de calcul
  - __presenceConstraint__ : (cf. [presenceConstraint](presenceConstraint))

### Les colonnes dynamiques
__dynamicColumns :__ Une _dynamicColumns_ est un ensemble de colonnes dont le nom est la concaténation d'un préfixe et d'une valeur dans un référentiel. 
  
  Par exemple s'il existe un référentiel "propriétés" avec les valeurs (couleur, catégorie, obligatoire), on pourrait avoir dans un autre référentiel (en utilisant le préfixe "pts_") pts_couleur, pts_catégorie et pts_obligatoire, en les déclarant comme _dynamicColumns_.

- __headerPrefix :__ on définit la chaine de caractères servant à identifier les colonnes dynamique se trouvant dans le fichier
- __internationalizationName :__ (cf. [internationalizationName](internationalizationName))
- __presenceConstraint__ :(cf. [presenceConstraint](presenceConstraint))
- __reference:__ nom de la référence listant les colonnes dynamiques attendues
- __referenceColumnToLookForHeader :__ nom de la colonne listant les noms de colonnes dynamiques attendue 

### Les validateur<a id = "checker"/> (checkers)
__checker:__ c'est la section où on réalise les validations des données et leur typage.

- __name:__ il y a plusieurs vérifications possibles :

  - vérifier la nature type d'un champ (float, integer, date)  ( Integer, Float, Date)
  - vérifier que la valeur vérifie une expression régulière ( RegularExpression)
  - ajouter un lien avec un référentiel (Reference)
  - vérifier un script
    - le script renvoie true ( groovy.expression)
    - le script renvoie une valeur (`defaultValue`, `computation.expression`, `transformation.groovy`)

- Contenu de la section __params__ :

| name           | References | Integer | Float | Date | GroovyExpression | RegularExpression | *                                                                      |
|----------------|-----------|--------|------|------|------------------|-------------------|------------------------------------------------------------------------|
| refType        | X         |        |      |      |                  |                   | Le référentiels de jointure                                            |
| pattern        |           |        |      |      |                  | X                 | Le pattern pour une expression régulière                               |
| transformation | X         | X      | X    | X    | X                | X                 | La définition d'une transformation à faire avant de vérifier la valeur |
| required       | X         | X      | X    | X   | X    | X                | La valeur ne peut être nulle (true)                                    |
| multiplicity   | X         |        |      |      |      |                  | La colonne contient un tableau de référence (true)                     |
| groovy         |           |        |       |     | X    |                  | La définition d'une expression groovy                                  |
| duration       |           |        |      | X |         |                  | Pour une date la durée de cette date                                   |

- __multiplicity__ : La colonne est une valeur (**ONE**), ou un ensemble de valeurs séparées par une virgule (**MANY** ex: 42,36  chat,chien)
- __duration__: pour un checker **Date**, utilisé comme information temporelle (**timeScope**), on peut préciser la durée de la mesure.
> Une durée est définie au sens SQL d'un [interval](https://www.postgresql.org/docs/current/functions-datetime.html#OPERATORS-DATETIME-TABLE) ('1 HOUR', '2 WEEKS', '30 MINUTES').
- __groovy__: (cf. [groovy](groovy)) pour vérifier la valeur avec un script groovy.
- __pattern__: soit une expression régulière, soit une expression de format de date.
- __reftype__: le nom d'un référentiel
- __required__: true si la valeur doit être fournie

On peut rajouter une section __transformation__ pour modifier la valeur avant sa vérification :

Cette __transformation__ peut être configurée avec
- __codify :__ la valeur sera alors échappée pour être transformée en clé naturelle (Ciel orangé -> ciel_orange)
- __groovy :__ permet de déclarer une transformation de la valeur avec une expression Groovy (qui doit retourner une chaîne de caractère) (cf. [groovy](groovy))


La section <a id="groovy" /> groovy accepte trois paramètres
- __expression :__ une expression groovy (pour le checker GroovyExpression doit renvoyer true si la valeur est valide)
- __references :__ une liste de référentiels pour lesquels on veut disposer des valeurs dans l'expression
- __datatypes :__ une liste de datatypes pour lesquels on veut disposer des valeurs dans l'expression

> :alert : La différence entre une section groovy de la section params d'un checker __groovy__ et une section groovy de la section transformation de la section params, tient dans le fait que pour un checker groovy l'expression renvoyée est un booléen tandis que dans la transformation l'expression groovy renvoie une nouvelle valeur.
> 
> Les sections **defaultValue** sont des expressions groovy ; par example :
> - 42 
> - "quarante-deux" 
> - true
> - 9.8*datum.masse
> - datum.date.day+" "+datum.date.time"

  - __validations__: cette section permet de rajouter des validations sur plusieurs colonnes en même temps. On donnera un nom à chacune des validations en l'utilisant comme clef de la validation.
    - __internationalizationName :__ (cf. [internationalizationName](internationalizationName))
    - __checker__: (cf. [checker](checker))
    - __columns: un tableau de colonnes sur lesquelles porte la validation.

## Définition de référentiels hiérarchiques
__compositeReferences :__ Une référence composite est créée en indiquant un lien parent-enfant entre un ou plusieurs référentiels ou une récursion sur un référentiel. Cela permet de générer une clef hiérarchique qui sera utilisée pour afficher hiérarchiquement ces référentiels (par exemple pour les [authorizations](authorizations) ou pour le dépôt sur un [repository](repository)).

- __internationalizationName :__ (cf. [internationalizationName](internationalizationName))
  - __internationalizationName :__ (cf. [internationalizationName](internationalizationName))
  - __reference:__ Le nom du référentiel de jointure

  - __parentRecursiveKey :__ nom de la colonne parent se trouvant dans le même référentiel que la colonne enfant (dans le cas d'un référentiel qui fait référence à lui-même).

  - __parentKeyColumn :__ nom de la colonne parent se trouvant dans le référentiel enfant.

## Les types de données
__dataTypes :__

- __internationalizationName :__ (cf. [internationalizationName](internationalizationName))

- __data :__ description de l'organisation de l'enregistrement des données dans la base de données.

  Les données du fichier sont enregistrées dans des __[components](components)__ et sont liées soit à des informations des colonnes, soit à des informations de l'en-tête. On peut aussi générer des données calculées (__[computedComponents](computedComponents)__)

  - __components<a id="components" /> :__ 
    - __checker__ (cf. [checker](checker))
    - __defaultValue__ (cf. [groovy](groovy))
  - __computedComponents<a id="computedComponents" /> :__
    - __checker__ (cf. [checker](checker))
    - __computation :__ (cf. [computation](computation))


- __format :__ description du format du fichier

  - __constants :__ définition des informations présentes dans l'en-tête du fichier

    - __rowNumber :__ numéro de la ligne
    - __columnNumber :__ numéro de la colonne
    - __exportHeader :__ Un nom de colonne fictif (pour les expressions groovy)
    - __headerName __: Pour les informations présentes sous la ligne d'en-tête le nom de la colonne (à la place du numéro de la colonne) ; par exemple pour une ligne d'unité ou de contraintes.
    - __boundTo__: <a id="boundTo" /> permet de relier l'information à une composante de variable.
  - __headerLine :__ numéro de la ligne des noms des colonnes
  - __firstRowLine :__ numéro de la première ligne de données du tableau
  - __columns :__ Liste des colonnes du fichier CSV de données

    - __header :__ "nom de la colonne se trouvant dans le fichier"
    - __boundTo :__ (cf. [boundTo](boundTo)).
  - __repeteadColumns __: Définit des colonnes dont le nom répond à un pattern donné contenant des informations.
    - __boundTo :__ (cf. [boundTo](boundTo)).
    - __exportHeader :__ Un nom de colonne fictif (pour les expressions groovy)
    - __headerPattern __: une expression régulière dont les groupes sont liés à des composantes de variables dans la section tokens
      - __tokens__: un tableau de token dans l'ordre de leur groupe dans l'expression régulière. La valeur du groupe sera utilisée comme valeur de la colonne fictive.
        - __boundTo :__ (cf. [boundTo](boundTo)).
        - __exportHeader :__ Un nom de colonne fictif (pour les expressions groovy)
- __uniqueness :__ C'est là qu'on définit une contrainte d'unicité, en listant la liste des _variable components_ qui composent la clef. Si un fichier possède des lignes en doublon avec lui-même il sera rejeté.

- __validations__: Cette section permet de rajouter des validations sur les données. On donnera un nom à chacune des validations en l'utilisant comme clef.
  - __checker__: (cf. [checker](checker))
  - __internationalizationName :__ (cf. [internationalizationName](internationalizationName))
- __authorization<a id="authorizations" /> :__ Cette section permet de définir les informations sur lesquelles on posera les droits. Elle est aussi utilisée pour le dépôt sur un [repository](repository)
   - __dataGroups :__ Un regroupement des variables (projection) pour leur donner des droits différents. Une ligne du fichier est découpée en autant de lignes qu'il y a de *dataGroups*.

     - __data :__ Liste des noms des `variables` définis dans `data`
     - __label :__ nom du groupe de données
    
  - __authorizationScopes :__ On définit des composantes de contexte. Elles doivent être liées à des référentiels composites (checker Reference) ce qui permettra de les sélectionner dans un arbre pour limiter la portée de l'autorisation. On donnera un nom à chaque `authorizationScopes en l'utilisant comme clef.
    - __variable :__ nom d'un _data_
    - __component :__ nom d'un _component_ dans un _data_
    - __internationalizationName: (cf. [internationalizationName](internationalizationName)) 
  
  - __timeScope :__ On définit des composantes de temporelles (checker Date) pour limiter les autorisations à des intervales de dates. Si les valeurs correspondent à une agrégation temporelle, il faudra préciser la durée de la période de aggregation dans le champ duration du checker (par défaut 1 jour).
  - __columnsDescription __: décrit comment dans le panneau des autorisations, chacun des rôles (depot, suppression publication..), doit être affiché. 
    - __display__: visibilité de la 
    - __title__: le nom du rôle
    - __internationalizationName__: (cf. [internationalizationName](internationalizationName))
    - __withDatagroups __: true si le rôle peut ne porter que sur certaines variables
    - __withPeriods __: true si le rôle peut être limité à certaines périodes.
  Par défaut, la configuration suivante sera appliquée pour afficher les autorisations. Vous devrez la redéfinir entièrement pour lui apporter des changements : 
  
```yaml
---
admin:
  display: true
  title: admin
  withPeriods: false
  withDataGroups: false
  internationalizationName:
    en: Administration
    fr: Administration
delete:
  display: true
  title: delete
  withPeriods: false
  withDataGroups: false
  internationalizationName:
    en: Deletion
    fr: Suppression
depot:
  display: true
  title: depot
  withPeriods: false
  withDataGroups: false
  internationalizationName:
    en: Deposit
    fr: Dépôt
extraction:
  display: true
  title: extraction
  withPeriods: true
  withDataGroups: true
  internationalizationName:
    en: Extraction
    fr: Extraction
publication:
  display: true
  title: publication
  withPeriods: false
  withDataGroups: false
  internationalizationName:
    en: Publication
    fr: Publication
```

- __repository :__ Permet la gestion du dépôt des fichiers par période et contexte en se basant sur les informations de la section  (__[authorization](authorizations)__)

  - __filePattern :__ Permet de définir une expression régulière pour remplir les champs du formulaire automatiquement. Chaque "groupe de l'expression correspond à un __token__ ou un __authorizationScope__, qu'on ordonne
  - __authorizationScope :__ Permet de définir quel groupe correspond à quel __authorizationScopes__ (la clef est le nom de l'__authorizationScopes__)

  - __startDate :__ Section de la date de début
    - __token : numéro du groupe ()
  - __endDate :__ Section de la date de fin
    - __token : numéro du groupe ()
